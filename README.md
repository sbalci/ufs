
<!-- badges: start -->

[![Pipeline
status](https://gitlab.com/r-packages/ufs/badges/master/pipeline.svg)](https://gitlab.com/r-packages/ufs/commits/master)
[![Coverage
status](https://codecov.io/gl/r-packages/ufs/branch/master/graph/badge.svg)](https://codecov.io/gl/r-packages/ufs?branch=master)
[![Version on
CRAN](https://www.r-pkg.org/badges/version/ufs?color=brightgreen)](https://cran.r-project.org/package=ufs)
[![Monthly downloads on
CRAN](https://cranlogs.r-pkg.org/badges/last-month/ufs?color=brightgreen)](https://cran.r-project.org/package=ufs)
[![Total downloads on
CRAN](https://cranlogs.r-pkg.org/badges/grand-total/ufs?color=brightgreen)](https://cran.r-project.org/package=ufs)
<!-- badges: end -->

# <img src='img/hex-logo.png' align="right" height="200" /> ufs 📦

ufs: A leaner, cleaner version of the userfriendlyscience package

The pkgdown website for this project is located at
<https://r-packages.gitlab.io/ufs>.

## Installation

You can install the released version of `ufs` from
[CRAN](https://CRAN.R-project.org) with:

``` r
install.packages('ufs');
```

You can install the development version of `ufs` from
[GitLab](https://gitlab.com) with:

``` r
devtools::install_gitlab('r-packages/ufs');
```

(assuming you have `devtools` installed; otherwise, install that first
using the `install.packages` function)
