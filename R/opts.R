#' Options for the ufs package
#'
#' The `ufs::opts` object contains three functions to set, get, and reset
#' options used by the ufs package. Use `ufs::opts$set` to set options,
#' `ufs::opts$get` to get options, or `ufs::opts$reset` to reset specific or
#' all options to their default values.
#'
#' It is normally not necessary to get or set `ufs` options.
#'
#' The following arguments can be passed:
#'
#' \describe{
#'   \item{...}{For `ufs::opts$set`, the dots can be used to specify the options
#'   to set, in the format `option = value`, for example,
#'   `varViewCols = c("values", "level")`. For
#'   `ufs::opts$reset`, a list of options to be reset can be passed.}
#'   \item{option}{For `ufs::opts$set`, the name of the option to set.}
#'   \item{default}{For `ufs::opts$get`, the default value to return if the
#'   option has not been manually specified.}
#' }
#'
#' The following options can be set:
#'
#' \describe{
#'
#'   \item{varViewCols}{The order and names of the columns to include in the
#'   variable view.}
#'
#'   \item{showLabellerWarning}{Whether to show a warning if labeller labels
#'   are encountered.}
#'
#' }
#'
#' @aliases opts set get reset
#'
#' @usage opts
#'
#' @examples ### Get the default columns in the variable view
#' ufs::opts$get(varViewCols);
#'
#' ### Set it to a custom version
#' ufs::opts$set(varViewCols = c("values", "level"));
#'
#' ### Check that it worked
#' ufs::opts$get(varViewCols);
#'
#' ### Reset this option to its default value
#' ufs::opts$reset(varViewCols);
#'
#' ### Check that the reset worked, too
#' ufs::opts$get(varViewCols);
#'
#' @export
opts <- list();

opts$set <- function(...) {
  dots <- list(...);
  dotNames <- names(dots);
  names(dots) <-
    paste0("ufs.", dotNames);
  if (all(dotNames %in% names(opts$defaults))) {
    do.call(options,
            dots);
  } else {
    stop("Option '", option, "' is not a valid (i.e. existing) option for ufs!");
  }
}

opts$get <- function(option, default=FALSE) {
  option <- as.character(substitute(option));
  if (!option %in% names(opts$defaults)) {
    stop("Option '", option, "' is not a valid (i.e. existing) option for ufs!");
  } else {
    return(getOption(paste0("ufs.", option),
                     opts$defaults[[option]]));
  }
}

opts$reset <- function(...) {
  optionNames <-
    unlist(lapply(as.list(substitute(...())),
                  as.character));
  if (length(optionNames) == 0) {
    do.call(opts$set,
            opts$defaults);
  } else {
    prefixedOptionNames <-
      paste0("ufs.", optionNames);
    if (all(optionNames %in% names(opts$defaults))) {
      do.call(opts$set,
              opts$defaults[optionNames]);
    } else {
      invalidOptions <-
        !(optionNames %in% names(opts$defaults));
      stop("Option(s) ", vecTxtQ(optionNames[invalidOptions]),
           "' is/are not a valid (i.e. existing) option for ufs!");
    }
  }
}

opts$defaults <-
  list(

    ### Regular expressions to use when parsing LimeSurvey
    ### labels in processLSvarLabels

    labelExtractionRegExPair = c("\\[(.*)\\].*", "\\1"),

    leftAnchorRegExPairs = list(
      c(".*[[:graph:]]\\s*([A-Z][a-z][^|]*)\\s*\\|\\s*(.+)",
        "\\1"),
      c(".*\\S\\.\\.\\.(\\S[^|]+)\\|(.+)",
        "\\1"),
      c(".*\\.([^|]+)\\|(.+)",
        "\\1"),
      c(".*…(\\S[^|]+)\\s*\\|\\s*(.+)",
        "\\1"),
      c(".*:([^|]+)\\s*\\|\\s*(.+)",
        "\\1"),
      c(".*\\?([^|]+)\\s*\\|\\s*(.+)",
        "\\1"),
      c(".*\\S\u2026(\\S[^|]+)\\|(.+)",
        "\\1")
    ),

    rightAnchorRegExPairs = list(
      c(".*[[:graph:]]\\s*([A-Z][a-z][^|]*)\\s*\\|\\s*(.+)",
        "\\2"),
      c(".*\\.\\.\\.([^|]+)\\|(.+)",
        "\\2"),
      c(".*\\.([^|]+)\\|(.+)",
        "\\2"),
      c(".*:([^|]+)\\s*\\|\\s*(.+)",
        "\\2"),
      c(".*…([^|]+)\\s*\\|\\s*(.+)",
        "\\2"),
      c(".*\\?([^|]+)\\s*\\|\\s*(.+)",
        "\\2"),
      c(".*\\S\u2026(\\S[^|]+)\\|(.+)",
        "\\2")
    ),

    ### Where to print tables; 'console', 'viewer', and/or
    ### one or more filenames in existing directories
    tableOutput = c("console", "viewer"),

    ### CSS for tableOutput
    tableOutputCSS = paste0("<style>",
                            "p,th,td{font-family:sans-serif}",
                            "td{padding:3px;vertical-align:top;}",
                            "tr:nth-child(even){background-color:#f2f2f2}",
                            "</style>"),

    ### color to use for the background when exporting to html
    exportHTMLbackground = "white",

    ### Whether you want extra information, as for debugging
    debugging = FALSE

  )

