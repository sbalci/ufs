#' Vargha & Delaney's A
#'
#' @param control A vector with the data for the control condition.
#' @param experimental A vector with the data from the experimental condition.
#' @param warn Whether to allow the [stats::wilcox.test()] function to emit
#' warnings, for example if ties are encountered.
#'
#' @return A numeric vector of length 1 with the A value, named 'A'.
#' @export
#'
#' @examples ufs::A_VarghaDelaney(1:8, 3:12);
A_VarghaDelaney <- function(control,
                            experimental,
                            warn = FALSE) {
  n1 <- length(control);
  n2 <- length(experimental);
  if (warn) {
    W <- stats::wilcox.test(experimental, control)$statistic;
  } else {
    suppressWarnings(W <- stats::wilcox.test(experimental, control)$statistic);
  }
  U <- (n1 * n2) - W;
  return(setNames(((n1 * n2) - U) /
                    (n1 * n2),
                  'A'));
}
